use actix_web::{App, HttpResponse, HttpServer, Responder, get};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
struct Dogs {
    names: Vec<String>
}

#[get("/")]
fn index() -> impl Responder {
    HttpResponse::Ok().body("Welcome to the Rust API!")
}

#[get("/dogs")]
fn get_dogs() -> impl Responder {
    let dogs = Dogs {names: vec![String::from("Atticus"), String::from("Odin")]};
    HttpResponse::Ok()
        .json(dogs.names)
}

fn main() {
    HttpServer::new(|| {
        App::new()
            .service(index)
            .service(get_dogs)
    })
    .bind("127.0.0.1:5001")
    .unwrap()
    .run()
    .unwrap();
}
