import { Component, OnInit } from '@angular/core';
import { RustService } from './_services/rust.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'rust-client';
  dogNames: Array<string>;

  constructor(private rustService: RustService) {}

  ngOnInit(): void {
    this.dogNames = this.rustService.getDogNames();
  }
}
