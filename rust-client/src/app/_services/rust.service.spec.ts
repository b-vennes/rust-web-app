/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RustService } from './rust.service';

describe('Service: Rust', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RustService]
    });
  });

  it('should ...', inject([RustService], (service: RustService) => {
    expect(service).toBeTruthy();
  }));
});
