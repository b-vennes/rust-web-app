import { Injectable } from '@angular/core';
import { get_dog_names } from '../_wasm';

@Injectable({
  providedIn: 'root'
})
export class RustService {

  constructor() { }

  getDogNames(): Array<string> {
    return get_dog_names().split(',');
  }
}
