use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn get_dog_names() -> String {
    vec!["Atticus".to_string(), "Odin".to_string()]
        .join(",")
}
